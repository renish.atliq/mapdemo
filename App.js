import React, {useEffect, useState} from 'react';
import type {Node} from 'react';
import {View, SafeAreaView, StyleSheet} from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {Map} from './App/Component/Map';
import {hp} from './App/Helper/responsive';

let watchID;
const App: () => Node = () => {
  const [region, setRegion] = useState();
  const [address, setAddress] = useState('');

  const getCurrentPosition = () => {
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
      interval: 10000,
      fastInterval: 5000,
    })
      .then(() => {
        watchID = Geolocation.getCurrentPosition(
          position => {
            const {latitude, longitude} = position.coords;
            setRegion({
              latitude,
              longitude,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            });
            console.log(position);
          },
          error => {
            console.log(error);
          },
          {
            enableHighAccuracy: false,
            useSignificantChanges: true,
            timeout: 15000,
          },
        );
      })
      .catch(err => {
        alert('Please allow the location to get the current location');
      });
  };
  useEffect(() => {
    getCurrentPosition();
    return () => {
      Geolocation.clearWatch(watchID);
    };
  }, []);

  const getAddress = (data, details) => {
    // 'details' is provided when fetchDetails = true
    let placeId = details?.place_id;
    let apiKey = 'API_KEY';
    fetch(
      `https://maps.googleapis.com/maps/api/place/details/json?placeid=${placeId}&key=${apiKey}`,
    )
      .then(async res => {
        let addressJson = await res.json();
        let location = addressJson.result.geometry.location;
        let {lat, lng} = location;
        let latitude = lat;
        let longitude = lng;
        setAddress(addressJson.result.formatted_address);
        setRegion({
          latitude,
          longitude,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        });
      })
      .catch(err => {
        console.log(
          'Error while getting lattitude and longitude from place_id',
          err,
        );
      });
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{zIndex: 0}}>
        <Map
          top={hp(50)}
          radius={13}
          styles={{
            zIndex: 0,
            overflow: 'hidden',
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 4,
            },
            shadowOpacity: 0.3,
            shadowRadius: 4.65,
            elevation: 8,
          }}
          region={region}
          marker={true}
          showAddress={address => setAddress(address)}
        />
      </View>
      <GooglePlacesAutocomplete
        styles={{
          textInput: styles.textColor,
          description: styles.textColor,
          container: {
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            zIndex: 10,
          },
        }}
        placeholder="Search"
        textInputProps={{
          value: address,
          onChangeText: text => {
            console.log(text);
            setAddress(text);
          },
        }}
        onPress={(data, details = null) => getAddress(data, details)}
        onFail={error =>
          console.log('ERRO WHILE GETTING THE AUTO COMPLETE ADDRESS', error)
        }
        query={{
          key: 'API_KEY',
          language: 'en',
        }}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  input: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 10,
  },
  textColor: {color: 'black'},
});

export default App;
