import {Dimensions} from 'react-native';

function wp(width) {
  return (Dimensions.get('window').width * width) / 375;
}

function hp(height) {
  return (Dimensions.get('window').height * height) / 812;
}

export {wp, hp};
