import axios from 'axios';
import React from 'react';
import {View} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import {get} from '../Helper/ApiHelper';
import {wp, hp} from '../Helper/responsive';

export const Map = props => {
  const {region, marker, top, marginH, radius, styles} = props;
  return (
    <View
      style={{
        marginHorizontal: marginH ? marginH : wp(20),
        marginTop: top ? top : 0,
        borderRadius: radius ? radius : 0,
        ...styles,
      }}>
      <MapView
        onPress={e => {
          console.log(
            'Coordinates from Map location click',
            e.nativeEvent.coordinate,
          );
          let apiKey = 'API_KEY';
          fetch(
            `https://maps.googleapis.com/maps/api/geocode/json?latlng=${e.nativeEvent.coordinate.latitude},${e.nativeEvent.coordinate.longitude}&key=${apiKey}`,
          )
            .then(async res => {
              let placeInfo = await res.json();
              props.showAddress(placeInfo['results'][0]['formatted_address']);
            })
            .catch(err => {
              console.log(
                'Error while google place auto complete address from lattitude and longitude',
                err,
              );
            });
          //   get(
          //     `https://maps.googleapis.com/maps/api/geocode/json?latlng=${e.nativeEvent.coordinate.latitude},${e.nativeEvent.coordinate.longitude}&key=${apiKey}`,
          //   )
          //     .then(async res => {
          //       let placeInfo = res.data;
          //       console.log(
          //         'placeInfo',
          //         placeInfo['results'][0]['formatted_address'],
          //       );
          //       props.showAddress(placeInfo['results'][0]['formatted_address']);
          //     })
          //     .catch(err => {
          //       console.log(
          //         'Error while google place auto complete address from lattitude and longitude',
          //         err,
          //       );
          //     });
        }}
        style={{
          height: hp(160),
        }}
        region={region}>
        {marker && region && (
          <Marker
            draggable
            coordinate={{
              latitude: region.latitude,
              longitude: region.longitude,
            }}
            title={marker.title}
            description={marker.description}
          />
        )}
      </MapView>
    </View>
  );
};
